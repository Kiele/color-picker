import gulp from 'gulp';
import sass from 'gulp-sass';
import del from 'del';
import cleanCSS from 'gulp-clean-css';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'gulp-autoprefixer';
import browserSync from 'browser-sync';
import babel from 'gulp-babel';
import uglify from 'gulp-uglify';

gulp.task('sass',()=>{
    return gulp.src('./src/style/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(autoprefixer())
    .pipe(sass())
    .pipe(cleanCSS())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/style'))
    .pipe(browserSync.stream());
});

gulp.task('babel',()=>{
    return gulp.src('./src/**/*.js')
    .pipe(babel({
        presets: ['@babel/env']
    }))
    .pipe(uglify())
    .pipe(gulp.dest('./dist'))
});

gulp.task('clean',()=>{
    return del('./dist/**');
});

gulp.task('static',cb =>{
    gulp.src('./src/style/prism.css').pipe(gulp.dest('./dist/style'));
    gulp.src('./src/*.html').pipe(gulp.dest('./dist'));
    cb();
});

gulp.task('build', gulp.series(['clean','static','sass','babel'], function(){
    browserSync.init({
        server: "./dist"
    });
    gulp.watch('./src/style/**/*.scss', gulp.series(['sass']));
    gulp.watch('./src/**/*.html').on('change',gulp.series(['static'], function(){
        browserSync.reload();
    }));
    gulp.watch('./src/**/*.js').on('change',gulp.series(['babel'], function(){
        browserSync.reload();
    }));
}));

gulp.task('default', gulp.series(['build']));