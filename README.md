# Color picker ver. 1.0.0

## Getting started

To download plugin files visit [Documentation site](https://color-picker-bb561.firebaseapp.com)

## Installation

You'll need include CSS to the **Head** section

```
<link rel="stylesheet" href="picker.css">
```

Javascript code insert before closing **Body** tag

```
<script src="picker.js"></script>
```

## Basic usage

To initialize your color picker put the following code snippets

```
<input type="text" id="picker">
```

```
const picker = new ColorPicker({
    el:'#picker'
});
```


## Settings

```
const picker = new ColorPicker({
    el:'string',
    style:'object',
    format:'string',
    color:'string',
    palette:'array',
    selectText:'string',
    cancelText:'string',
    theme:'string',
    onChange:'function'
});
```


## Styling

```
const pickerStyle = new ColorPicker({
    el:'#picker-style',
    style:{
        color:'#ffffff',
        borderColor:'#7d95dd',
        backgroundColor:'#1d1d1d',
        margin:'0 0 20px 0'
    }
});
```


## Format and color

There's a posibility to define color and format, default is rgb(255,255,255)
Supported formats: **HEX**, **RGB**

```
const pickerHex = new ColorPicker({
    el:'#picker-hex',
    format:'hex',
    color:'#ff0000'
});
```

## Color palette

Plugin offers a palette with wide spectrum of colors, but you can define your own color palette.
Palette must be an **array**

```
const pickerPalette = new ColorPicker({
    el:'#picker-palette',
    palette:['rgb(255,0,0)',
        'rgb(0,255,0)',
        'rgb(0,0,255)']
});
```


## Buttons

Settings **selectText** and **cancelText** allows to set buttons content.

```
const pickerButtons = new ColorPicker({
    el:'#picker-buttons',
    cancelText:'Foo',
    selectText:'Bar'
});
```

## Themes

Using variable **theme** you can change the color theme.

Available themes:
*  Dark

```
const pickerThemes = new ColorPicker({
    el:'#picker-themes',
    theme:'dark'
});
```

## onChande method

Method onChange activates every time that **input value** is changing.
onChange must be a **function**

```
const pickerOnChange = new ColorPicker({
    el:'#picker-on-change',
    onChange(){
        console.log(this.input.value);
    }
});
```
## License

**This project is licensed under the MIT License - see the LICENSE file for details**






