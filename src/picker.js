class ColorPicker{
    constructor({el,color,palette,format,onChange,selectText,cancelText,theme,style}){
        el = document.querySelectorAll(el);
        el.forEach(item =>{
            let container = document.createElement('div');
            let input = document.createElement('input');
            let label = document.createElement('label');
            if(style){
                const {
                    type,
                    color,
                    backgroundColor,
                    borderColor,
                    margin
                } = style;
                input.style.color = style.color;
                input.style.backgroundColor = style.backgroundColor;
                input.style.borderColor = style.borderColor;
                container.style.margin = style.margin;
            }
            let defaultColor;
            format ? this.format = format : this.format = 'rgb'
            if(format === 'rgb' || !format)
                defaultColor = 'rgb(255,255,255)'
            else if(format === 'hex')
                defaultColor = '#ffffff'
            color ? this.color = input.value || color : this.color = defaultColor
            container.classList.add('input-color');
            input.setAttribute('id',item.getAttribute('id'));
            container.appendChild(input);
            container.appendChild(label);
            input.setAttribute('value',this.color);
            label.style.backgroundColor = this.color;
            item.replaceWith(container);
            selectText ? this.selectText = selectText : this.selectText = 'Select'
            cancelText ? this.cancelText = cancelText : this.cancelText = 'Cancel'
            if(theme === 'dark'){
                this.theme = theme;
            }else{
                this.theme = 'default'
            }
            this.palette = palette || [
                'rgb(255,204,204)','rgb(255,230,204)','rgb(255,255,204)','rgb(204,255,204)','rgb(204,255,230)','rgb(204,255,255)','rgb(204,230,255)','rgb(204,204,255)','rgb(230,204,255)','rgb(255,204,255)',
                'rgb(255,153,153)','rgb(255,206,153)','rgb(255,255,153)','rgb(153,255,153)','rgb(153,255,206)','rgb(153,255,255)','rgb(153,206,255)','rgb(153,153,255)','rgb(202,153,255)','rgb(255,153,255)',
                'rgb(255,102,102)','rgb(255,181,102)','rgb(255,255,102)','rgb(102,255,102)','rgb(102,255,181)','rgb(102,255,255)','rgb(102,181,255)','rgb(102,102,255)','rgb(176,102,255)','rgb(255,102,255)',
                'rgb(255,51,51)','rgb(255,156,51)','rgb(255,255,51)','rgb(51,255,51)','rgb(51,255,156)','rgb(51,255,255)','rgb(51,156,255)','rgb(51,51,255)','rgb(150,51,255)','rgb(255,51,255)','rgb(255,0,0)',
                'rgb(255,128,0)','rgb(255,255,0)','rgb(0,255,0)','rgb(0,255,128)','rgb(0,255,255)','rgb(0,128,255)','rgb(0,0,255)','rgb(128,0,255)','rgb(255,0,255)','rgb(204,0,0)',
                'rgb(204,105,0)','rgb(204,204,0)','rgb(0,204,0)','rgb(0,204,105)','rgb(0,204,204)','rgb(0,105,204)','rgb(0,0,204)','rgb(99,0,204)','rgb(204,0,204)',
                'rgb(153,0,0)','rgb(153,79,0)','rgb(153,153,0)','rgb(0,153,0)','rgb(0,153,79)','rgb(0,153,153)','rgb(0,79,153)','rgb(0,0,153)','rgb(74,0,153)','rgb(153,0,153)',
                'rgb(102,0,0)','rgb(102,53,0)','rgb(102,102,0)','rgb(0,102,0)','rgb(0,102,53)','rgb(0,102,102)','rgb(0,53,102)','rgb(0,0,102)','rgb(49,0,102)','rgb(102,0,102)',
                'rgb(51,0,0)','rgb(51,26,0)','rgb(51,51,0)','rgb(0,51,0)','rgb(0,51,26)','rgb(0,51,51)','rgb(0,51,51)','rgb(0,0,51)','rgb(25,0,51)','rgb(51,0,51)',
                'rgb(255,255,255)','rgb(230,230,230)','rgb(204,204,204)','rgb(179,179,179)','rgb(153,153,153)','rgb(128,128,128)','rgb(102,102,102)','rgb(77,77,77)','rgb(51,51,51)','rgb(0,0,0)'
            ];
            input.addEventListener('click',()=>{
                this.input = input;
                this.label = label;
                this.offset = container.getBoundingClientRect();
                this.container = container;
                this.init();
            });
            if(typeof onChange === 'function'){
                this.onChange = onChange;
            }
        });
    }
    init(){
        this.color = this.input.value;
        this.hexMatch = new RegExp(/^#[0-9a-f]{6}$/);
        this.rgbMatch = new RegExp(/rgb\(\s*(?:(?:\d{1,2}|1\d\d|2(?:[0-4]\d|5[0-5]))\s*,?){3}\)$/);
        let dragEl,offset;
        const picker = document.createRange().createContextualFragment(`
            <div class="color-picker ${this.theme}">
                <div class="color-values">
                    <input type="text" class="color-code-rgb">
                    <input type="text" class="color-code-hex">
                </div>
                <div class="color-preview">
                    <div class="start-color" style="background-color:${this.color}"></div>
                    <div class="current-color"></div>
                </div>
                <div class="palette color-palette"></div>
                <div class="palette saved-colors"></div>
                <div class="picker active">
                    <canvas class="color-picker-a" width="200" height="200"></canvas>
                    <canvas class="color-picker-b" width="36" height="200"></canvas>
                </div>
                <ul class="picker-menu">
                    <li id="picker" class="active"></li>
                    <li id="color-palette"></li>
                    <li id="saved-colors"></li>
                    <li id="random-color"></li>
                </ul>
                <button type="button" class="cancel-color">${this.cancelText}</button>
                <button type="button" class="select-color">${this.selectText}</button>
            </div>`
        );
        const container = picker.querySelector('.color-picker');
        const canContainer = picker.querySelector('.picker');
        const inputHex = picker.querySelector('.color-code-hex');
        const inputRgb = picker.querySelector('.color-code-rgb');
        const cColor = picker.querySelector('.current-color');
        const pickerA = picker.querySelector('.color-picker-a');
        const pickerB = picker.querySelector('.color-picker-b');
        const colorPalette = picker.querySelector('.color-palette');
        const savedColors = picker.querySelector('.saved-colors');
        const scPalette = picker.querySelectorAll('.palette');
        const pickerMenu = picker.querySelector('.picker-menu');
        const cancel = picker.querySelector('.cancel-color');
        const select = picker.querySelector('.select-color');
        const ctx = pickerA.getContext('2d');
        const ctx2 = pickerB.getContext('2d');
        this.fillGradient(ctx,this.color);
        const grd = ctx2.createLinearGradient(36,0,36,200);
        grd.addColorStop(0.06, 'rgb(255,0,0)');
        grd.addColorStop(0.21, 'rgb(255,255,0)');
        grd.addColorStop(0.39, 'rgb(0,255,0)');
        grd.addColorStop(0.53, 'rgb(0,255,255)');
        grd.addColorStop(0.67, 'rgb(0,0,255)');
        grd.addColorStop(0.80, 'rgb(255,0,255)');
        grd.addColorStop(0.96, 'rgb(255,0,0)');
        ctx2.fillStyle = grd;
        ctx2.fillRect(0,0,36,200);
        this.changeValue(this.color,inputHex,inputRgb,cColor);
        const box = document.createElement('div');
        box.classList.add('mw-cp');
        document.body.appendChild(box);
        this.container.appendChild(picker);

        //Pozycjonowanie

        const cOffset = container.getBoundingClientRect();
        cOffset.bottom>window.innerHeight ? container.style.bottom = `${this.offset.height}px` : container.style.top = `${this.offset.height}px`

        //tworzenie palety

        this.palette.forEach(item =>{
            const element = document.createElement('div');  
            element.style.backgroundColor = item;
            element.classList.add('color');
            colorPalette.appendChild(element);
        });

        //localStorage
        let saved = localStorage.getItem('color-palette');
        if(saved){
            let pal = localStorage.getItem('color-palette');
            pal = pal.split(',');
            pal.forEach(item =>{
                const element = document.createElement('div');  
                element.style.backgroundColor = item;
                element.classList.add('color');
                savedColors.appendChild(element);
            });
        }else{
            localStorage.setItem('color-palette','');
        }

        //przyciski

        cancel.addEventListener('click',()=>{
            box.remove();
            container.remove();
            this.changeValue(this.color,inputHex,inputRgb,cColor);
        },false);
        select.addEventListener('click',() =>{
            box.remove();
            container.remove();
            let saved = localStorage.getItem('color-palette');
            if(saved){
                if(!saved.includes(inputHex.value)){
                    let palette = [];
                    palette.push(saved,inputHex.value);
                    localStorage.setItem('color-palette',palette.join(','));
                }
            }else{
                localStorage.setItem('color-palette',inputHex.value);
            }
        },false);

        //box

        box.addEventListener('click',()=>{
            select.click();
        },false);


        pickerMenu.addEventListener('click',e =>{
            if(e.target.nodeName==='LI'){
                const elements = pickerMenu.querySelectorAll('li');
                const containers = document.querySelectorAll('.picker, .saved-colors, .color-palette');
                const id = e.target.getAttribute('id');
                const el = document.querySelector(`.${id}`);
                containers.forEach(item =>{
                    item.classList.remove('active');
                });
                elements.forEach(item =>{
                    item.classList.remove('active');
                });
                if(id==='random-color'){
                    const main = document.querySelectorAll('.picker, #picker');
                    let color;
                    main.forEach(item =>{
                        item.classList.add('active');
                    });
                    if(this.format === 'hex'){
                        color = this.randomColor(color);
                    }else{
                        color = this.hexToRgb(this.randomColor());
                    }
                    this.fillGradient(ctx,color);
                    this.changeValue(color,inputHex,inputRgb,cColor);
                }else{
                    e.target.classList.add('active');
                    el.classList.add('active');
                }
            }
        },false);
        const getColor = (element,x,y)=>{
            if(element.classList.contains('color-picker-a')){
                let imageData = ctx.getImageData(x,y,1,1).data;
                var color = `rgb(${imageData[0]},${imageData[1]},${imageData[2]})`;
            }else{
                let imageData = ctx2.getImageData(x,y,1,1).data;
                var color = `rgb(${imageData[0]},${imageData[1]},${imageData[2]})`;
                this.fillGradient(ctx,color);
            }
            if(this.format === 'hex'){
                color = this.rgbToHex(color);
            }
            this.changeValue(color,inputHex,inputRgb,cColor);
        };
        canContainer.addEventListener('mousedown',e =>{
            dragEl = e.target;
            if(dragEl.nodeName==='CANVAS'){
                offset = dragEl.getBoundingClientRect();
                const grabX = e.clientX - offset.left;
                const grabY = e.clientY - offset.top;
                getColor(dragEl,grabX,grabY);
            }
        });
        document.addEventListener('mousemove',e =>{
            if(!dragEl)
                return;
            let grabX = e.clientX - offset.left;
            let grabY = e.clientY - offset.top;
            if(grabX<=0)
                grabX = 0;
            if(grabY<=0)
                grabY = 0;
            if(grabX>=offset.width)
                grabX = offset.width-1;
            if(grabY>=offset.height)
                grabY = offset.height-1;
            getColor(dragEl,grabX,grabY);
        });
        document.addEventListener('mouseup',()=>{
            dragEl = null;
        });
        scPalette.forEach(item =>{
            item.addEventListener('click',e =>{
                const element = e.target;
                const elements = item.querySelectorAll('div');
                if(element.classList.contains('color')){
                    let color;
                    if(this.format === 'hex'){
                        color = this.rgbToHex(element.style.backgroundColor);
                    }else{
                        color = element.style.backgroundColor;
                    }
                    color = color.replace(/\s+/g, '');
                    elements.forEach(item=>{
                        item.style.border = '';
                    });
                    element.style.border = `inset 2px ${color}`;
                    this.fillGradient(ctx,color);
                    this.changeValue(color,inputHex,inputRgb,cColor);
                }
            },false);
        });
        inputRgb.addEventListener('keyup',()=>{
            let color = inputRgb.value;
            if(this.rgbMatch.test(color)){
                if(this.format === 'hex'){
                    color = this.rgbToHex(color);
                }
                this.fillGradient(ctx,color);
                this.changeValue(color,inputHex,inputRgb,cColor);
            }
        });
        inputHex.addEventListener('keyup',()=>{
            let color = inputHex.value;
            if(this.hexMatch.test(color)){
                if(this.format === 'rgb'){
                    color = this.hexToRgb(color);
                }
                this.fillGradient(ctx,color);
                this.changeValue(color,inputHex,inputRgb,cColor);
            }
        });
    }
    fillGradient(ctx,color){
        ctx.fillStyle = color;
        ctx.fillRect(0,0,200,200);
        const grd2white = ctx.createLinearGradient(0,0,200,0);
        grd2white.addColorStop(1,'rgba(255,255,255,0)');
        grd2white.addColorStop(0,'rgba(255,255,255,1)');
        ctx.fillStyle = grd2white;
        ctx.fillRect(0,0,200,200);
        const grd2black = ctx.createLinearGradient(0,0,0,200);
        grd2black.addColorStop(0,'rgba(0,0,0,0)');
        grd2black.addColorStop(1,'rgba(0,0,0,1)');
        ctx.fillStyle = grd2black;
        ctx.fillRect(0,0,200,200);
    }
    rgbToHex(rgb){
        rgb = rgb.match(/^rgb?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
        return (rgb && rgb.length === 4) ? "#"+
            ("0"+parseInt(rgb[1],10).toString(16)).slice(-2)+
            ("0"+parseInt(rgb[2],10).toString(16)).slice(-2)+
            ("0"+parseInt(rgb[3],10).toString(16)).slice(-2):'';
    }
    randomColor(){
        return `#${Math.random().toString(16).substr(3,6)}`;
    }
    hexToRgb(hex){
        return 'rgb('+hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i
        ,(m,r,g,b) => '#'+r+r+g+g+b+b)
        .substring(1).match(/.{2}/g)
        .map(x => parseInt(x,16)).join(',')+')';
    }
    changeValue(color,hex,rgb,preview){
        if(this.format === 'hex'){
            hex.value = color;
            rgb.value = this.hexToRgb(color);
        }
        if(this.format === 'rgb'){
            rgb.value = color;
            hex.value = this.rgbToHex(color);
        }
        preview.style.backgroundColor = color;
        this.input.setAttribute('value',color);
        this.input.value = color;
        this.label.style.backgroundColor = color;
        if(this.onChange){
            this.onChange();
        }
    }
}