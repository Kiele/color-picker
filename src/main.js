const menuTrigger = document.querySelector('.menu-open');
const menuClose = document.querySelector('.menu-close');
const menu = document.querySelector('.page-navigation');
menuTrigger.addEventListener('click',()=>{
    menu.classList.add('menu-opened');
});

menuClose.addEventListener('click',()=>{
    menu.classList.remove('menu-opened');
});

const picker = new ColorPicker({
    el:'#picker'
});

const pickerStyle = new ColorPicker({
    el:'#picker-style',
    style:{
        type:'',
        color:'#ffffff',
        borderColor:'#7d95dd',
        backgroundColor:'#1d1d1d',
        margin:'0 0 20px 0'
    }
});

const pickerHex = new ColorPicker({
    el:'#picker-hex',
    format:'hex',
    color:'#ff0000'
});

const pickerPalette = new ColorPicker({
    el:'#picker-palette',
    palette:['rgb(255,0,0)',
        'rgb(0,255,0)',
        'rgb(0,0,255)']
});

const pickerButtons = new ColorPicker({
    el:'#picker-buttons',
    cancelText:'Foo',
    selectText:'Bar'
});

const pickerThemes = new ColorPicker({
    el:'#picker-themes',
    theme:'dark'
});

let box = document.querySelector('.on-change-demo');

const pickerOnChange = new ColorPicker({
    el:'#picker-on-change',
    onChange(){
        box.style.backgroundColor = this.input.value;
    }
});